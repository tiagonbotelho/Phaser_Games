var game = new Phaser.Game(800, 600, Phaser.AUTO, 'phaser-example', { preload: preload, create: create, update: update});

//variables
var player;
var aliens;
var bullets;
var bulletTime = 0;
var cursors;
var fireButton;
var explosions;
var starfield;
var score = 0;
var scoreString = '';
var scoreText;
var lives;
var enemyBullet;
var firingTimer = 0;
var stateText;
var livingEnemies = [];

//standart preload create update functions
function  preload() {
    game.load.image('ship', 'assets/img/player.png');
    game.load.image('bullet', 'assets/img/bullet.png');
    game.load.image('enemyBullet', 'assets/img/enemy-bullet.png');
    game.load.image('starfield', 'assets/img/starfield.png');
    game.load.spritesheet('kaboom', 'assets/img/explode.png', 128, 128);
    game.load.spritesheet('invader', 'assets/img/invader32x32x4.png', 32, 32);
}

function create() {
    game.physics.startSystem(Phaser.Physics.ARCADE);

    starfield = game.add.tileSprite(0, 0, game.world.width, game.world.height, 'starfield');

    //player
    player = game.add.sprite(400, 500, 'ship'); //400 and 500 so you can have it centered and below the middle in height
    player.anchor.setTo(0.5, 0.5);
    game.physics.enable(player, Phaser.Physics.ARCADE);

    //enemy group
    aliens = game.add.group();
    aliens.enableBody = true;
    aliens.physicsBodyType = Phaser.Physics.ARCADE;

    createAliens();

    //explosions
    explosions = game.add.group();
    explosions.createMultiple(30, 'kaboom');
    explosions.forEach(setupInvader, this);

    //hero bullet
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(30, 'bullet');
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 1);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);

    //enemy bullets
    enemyBullets = game.add.group();
    enemyBullets.enableBody = true;
    enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets.createMultiple(30, 'enemyBullet');
    enemyBullets.setAll('anchor.x', 0.5);
    enemyBullets.setAll('anchor.y', 1);
    enemyBullets.setAll('outOfBoundsKill', true);
    enemyBullets.setAll('checkWorldBounds', true);

    //Text
    stateText = game.add.text(game.world.centerX, game.world.centerY, '', {font: '84px Arial', fill: '#fff'});
    stateText.anchor.setTo(0.5, 0.5);
    stateText.visible = false;

    //Score
    scoreString = 'Score: ';
    scoreText = game.add.text(10, 10, scoreString + score, {font: '34px Arial', fill: '#fff'});

    //Lives
    lives = game.add.group();
    game.add.text(game.world.width - 100, 10, 'Lives: ', {font: '34px Arial', fill: '#fff'});

    for(var o = 0; o< 3; o++) {
        var ship = lives.create(game.world.width - 90 + (35 * o), 60, 'ship');
        ship.anchor.setTo(0.5, 0.5);
        ship.angle = 90;
        ship.alpha = 0.7;
    }

    //controls
    cursors = game.input.keyboard.createCursorKeys();
    fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
}

function update() {

    starfield.tilePosition.y +=2;

    if(player.alive) {
        //key pressed
        player.body.velocity.setTo(0, 0);

        if(cursors.left.isDown) {
            player.body.velocity.x = -200;
        } else if(cursors.right.isDown) {
            player.body.velocity.x = 200;
        }

        if(fireButton.isDown) {
            fireBullet();
        }

        if(game.time.now > firingTimer) {
            enemyFires();
        }
    }
    game.physics.arcade.overlap(bullets, aliens, collisionHandler, null, this);
    game.physics.arcade.overlap(enemyBullets, player, enemyHitsPlayer, null, this);

}

function createAliens() {

    for(var i = 0; i < 4; i++) {
        for(var j = 0; j < 10; j++) {
            var alien = aliens.create(j * 45, i * 50, 'invader');
            alien.anchor.setTo(0.5, 0.5);
            alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
            alien.play('fly');
            alien.body.moves = false;
        }
    }
    aliens.x = 50;
    aliens.y = 50;

    var tween = game.add.tween(aliens).to({x: 350}, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);
    tween.onLoop.add(function() {aliens.y += 10;}, this);
}

function setupInvader(invader) {
    invader.anchor.setTo(0.5, 0.5);
    invader.animations.add('kaboom');
}

function fireBullet() {
    if(game.time.now > bulletTime) {
        bullet = bullets.getFirstExists(false);
        if(bullet) {
            bullet.reset(player.x, player.y + 8);
            bullet.body.velocity.y = -600;
            bulletTime = game.time.now + 200;
        }
    }
}

function enemyFires() {
    enemyBullet = enemyBullets.getFirstExists(false);
    livingEnemies.length = 0;
    aliens.forEachAlive(function(alien) {
            livingEnemies.push(alien);
    });

    if(enemyBullet && livingEnemies.length > 0) {
        var random = game.rnd.integerInRange(0, livingEnemies.length - 1);
        var shooter = livingEnemies[random];
        enemyBullet.reset(shooter.body.x, shooter.body.y);
        game.physics.arcade.moveToObject(enemyBullet, player, 120);
        firingTimer += game.time.now + 2000;
    }
}

function collisionHandler (bullet, alien) {
    bullet.kill();
    alien.kill();

    score += 20;
    scoreText.text = scoreString + score;

    var explosion = explosions.getFirstExists(false);
    explosion.reset(alien.body.x, alien.body.y);
    explosion.play('kaboom', 30, false, true);
    if (aliens.countLiving() == 0)
    {
        score += 1000;
        scoreText.text = scoreString + score;

        enemyBullets.callAll('kill',this);
        stateText.text = " You Won, \n Click to restart";
        stateText.visible = true;

        game.input.onTap.addOnce(restart,this);
    }
}

function restart() {
    lives.callAll('revive');
    aliens.removeAll();
    createAliens();

    player.revive();
    stateText.visible = false;
}

function enemyHitsPlayer(player, bullet) {
    bullet.kill();
    live = lives.getFirstAlive();

    if(live) {
        //burns one live
        live.kill();
    }

    var explosion = explosions.getFirstExists(false);
    explosion.reset(player.body.x, player.body.y);
    explosion.play('kaboom', 30, false, true);

    if(lives.countLiving() < 1) {
        player.kill();
        enemyBullets.callAll('kill');

        stateText.text = "GAME OVER\n TAP TO PLAY AGAIN";
        stateText.visible = true;

        game.input.onTap.addOnce(restart, this);
    }
}
